#!/usr/bin/env python
import requests
import sys
import time
import random
from nslookup import Nslookup

#target_domain = "au-melbourne.privateinternetaccess.com"
#target_domain = "au-sydney.privateinternetaccess.com"
target_domain = "au-perth.privateinternetaccess.com"
dns_servers=["1.1.1.1"]
contact_email="test@test.net"
timeout=5.00

flag="f" # see https://getipintel.net/#web

dns_query = Nslookup(dns_servers=dns_servers)

def checkIP(ip,contact_email,timeout):
    #if you wish to use flags or json format, edit the request below
    result = requests.get("http://check.getipintel.net/check.php?ip="+ip+"&contact="+contact_email+"&flags="+flag+"s&format=json", timeout=timeout)

    if (result.status_code != 200):
        sys.stderr.write("ERROR: recieved " + result.status_code + "querying GetIPIntel")
        sys.exit(1)
    else:
        result_dict = result.json()
        return result_dict

def main():
    ips_record = dns_query.dns_lookup(target_domain)

    print("Querying each A record from " + target_domain + ":")
    
    # {'status': 'success', 'result': '1', 'queryIP': '27.50.82.131', 'queryFlags': 'bs', 'queryOFlags': None, 'queryFormat': 'json', 'contact': 'test@test.net'}
    for ip in ips_record.answer:
        record = checkIP(ip,contact_email,timeout)
        print("..queryIP: " + record['queryIP'] + " queryFlags: " + record['queryFlags'] + " result: " + record['result'])

        time.sleep(random.uniform(0.1, 1.2))
    
main()
