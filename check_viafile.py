#!/usr/bin/env python
import requests
import sys
import time
import random

file_path = "./nordvpn_au.txt"

contact_email="test@test.net"
timeout=5.00
flag="f" # see https://getipintel.net/#web


def checkIP(ip,contact_email,timeout):
    #if you wish to use flags or json format, edit the request below
    result = requests.get("http://check.getipintel.net/check.php?ip="+ip+"&contact="+contact_email+"&flags="+flag+"s&format=json", timeout=timeout)

    if (result.status_code != 200):
        sys.stderr.write("ERROR: recieved " + result.status_code + "querying GetIPIntel")
        sys.exit(1)
    else:
        result_dict = result.json()
        return result_dict

def main():

    print("Reading ip addresses from " + file_path + ":")

    with open(file_path) as file_in:
        lines = []
        for ip in file_in:
            record = checkIP(ip.rstrip(),contact_email,timeout)

            # {'status': 'success', 'result': '1', 'queryIP': '27.50.82.131', 'queryFlags': 'bs', 'queryOFlags': None, 'queryFormat': 'json', 'contact': 'test@test.net'}
            print("..queryIP: " + record['queryIP'] + " queryFlags: " + record['queryFlags'] + " result: " + record['result'])

            time.sleep(random.uniform(0.1, 1.2))
   
main()
